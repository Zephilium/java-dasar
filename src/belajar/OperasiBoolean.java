package belajar;

public class OperasiBoolean {

    public static void main(String[] args) {
        var absen = 70;
        var nilaiAkhir = 80;

        var lulus = absen >= 75 && nilaiAkhir >= 75;
        System.out.println(lulus);

        var lulus2 = absen >= 75 || nilaiAkhir >= 75;
        System.out.println(lulus2);
    }

}
