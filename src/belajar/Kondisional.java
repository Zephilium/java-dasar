package belajar;

public class Kondisional {

    public static void main(String[] args) {
        var nilai = 90;
        var absen = 80;

        var rata = (nilai + absen) / 2;
        System.out.println(rata);

        if (rata >= 80) {
            System.out.println("Nilai Anda BA");
        } else if (rata >= 70) {
            System.out.println("Nilai Anda B");
        } else if (rata >= 60) {
            System.out.println("Nilai Anda C");
        } else {
            System.out.println("Anda belum lulus");
        }
    }
}
