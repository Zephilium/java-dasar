package belajar;

public class Ternary {
    public static void main(String[] args) {
        var nilai = 50;
        String ucapan = nilai >= 75 ? "Selamat anda lulus" : "Anda tidak lulus";
        System.out.println(ucapan);

    }
}
