package belajar;

public class Array {

    public static void main(String[] args) {
        String[] arrayString = new String[3];

        arrayString[0] = "Fiqri";
        arrayString[1] = "Maulana";
        arrayString[2] = "Arokhman";

        System.out.println(arrayString[1]);

        int[] arrayInt = {
                10, 54, 23, 25
        };
        arrayInt[0] = 20;
        System.out.println(arrayInt[0]);
        System.out.println(arrayInt.length);

        String[][] members = {
                {"Fiqri", "Maulana", "Arokhman"},
                {"Budi", "Pambudi"},
        };

        System.out.println(members[0][1]);
        System.out.println(members[1][0]);
    }
}
