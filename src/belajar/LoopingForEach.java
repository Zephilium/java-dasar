package belajar;

public class LoopingForEach {
    public static void main(String[] args) {
        String[] names = {"Fiqri", "Maulana", "Arokhman"};


        for (int i = 0; i < names.length; i++) {
            System.out.println(names[i]);
        }
        System.out.println("");
        System.out.println("Menggunakan For Each");
        for (var name : names) {
            System.out.println(name);
        }
    }
}
