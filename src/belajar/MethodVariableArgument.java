package belajar;

public class MethodVariableArgument {
    public static void main(String[] args) {
        System.out.println("Tanpa Variable Argument");
        int[] nilai = {80, 60, 65, 90};
        withoutVariableArgument("Fiqri", nilai);

        System.out.println("Dengan Variable Argument");
        withVariableArgument("Budi", 70, 75, 90, 87);
    }

    static void withoutVariableArgument(String name, int[] values) {
        var total = 0;
        for (var value : values) {
            total += value;
        }
        var finalValue = total / values.length;

        if (finalValue >= 75) {
            System.out.println("Selamat " + name + " Anda Lulus");
        } else {
            System.out.println("Anda Belum Lulus");
        }

    }

    static void withVariableArgument(String name, int... values) {
        var total = 0;
        for (var value : values) {
            total += value;
        }
        var finalValue = total / values.length;

        if (finalValue >= 75) {
            System.out.println("Selamat " + name + " Anda Lulus");
        } else {
            System.out.println("Anda Belum Lulus");
        }

    }
}
