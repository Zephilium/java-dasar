package belajar;

public class Variable {

    public static void main(String[] args) {
        String name;
        name = "Fiqri Maulana Arokhman";

        int age = 21;

        String address = "Indonesia";

        System.out.println("Nama : " + name);
        System.out.println("Umur : " + age);
        System.out.println("Alamat : " + address);

        var nama = "Fiqri";
        System.out.println(nama);

        final double pi = 3.14; //Kata Kunci final supaya nilai nya engga bisa diubah
        System.out.println(pi);

    }
}
