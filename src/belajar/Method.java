package belajar;

import java.time.chrono.MinguoChronology;

public class Method {
    public static void main(String[] args) {
        sayHelloWorld();
        sayMyName("Fiqri", "Maulana");

        var hasil = sum(10, 50);
        System.out.println(hasil);
    }

    static void sayHelloWorld() {
        System.out.println("Hello World");
    }

    static void sayMyName(String firstName, String lastName) {
        System.out.println("Hello " + firstName + " " + lastName);
    }

    static int sum(int angka1, int angka2) {
        var total = angka1 + angka2;
        return total;
    }
}
