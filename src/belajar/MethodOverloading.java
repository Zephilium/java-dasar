package belajar;

public class MethodOverloading {
    public static void main(String[] args) {
        sayHello();
        sayHello("Budi");
    }

    static void sayHello() {
        System.out.println("Hello Fiqri");
    }

    static void sayHello(String firstname) {
        System.out.println("Hello " + firstname);
    }
}
