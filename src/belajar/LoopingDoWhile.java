package belajar;

public class LoopingDoWhile {
    public static void main(String[] args) {
        var counter = 100;
        do {
            System.out.println("Perulangan ke " + counter);
            counter++;
        } while (counter <= 10);

    }
}
