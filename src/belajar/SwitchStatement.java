package belajar;

public class SwitchStatement {

    public static void main(String[] args) {
        var nilai = "C";

        switch (nilai) {
            case "A":
                System.out.println("Lulus dengan baik");
                break;
            case "B":
            case "C":
                System.out.println("Anda Lulus");
                break;
            case "D":
                System.out.println("Lulus kurang baik");
                break;
            default:
                System.out.println("Anda tidak lulus");
        }
        switch (nilai) {
            case "A" -> System.out.println("Lulus dengan baik");
            case "B", "C" -> System.out.println("Anda Lulus");
            case "D" -> System.out.println("Lulus kurang baik");
            default -> {
                System.out.println("Anda tidak lulus"); //Bisa menggunakan kurung kurawal kalau perintah lebih dari 1 baris
            }
        }

        String ucapan = switch (nilai) {
            case "A":
                yield "Lulus dengan baik";
            case "B", "C":
                yield "Lulus";
            case "D":
                yield "Lulus kurang baik";
            default:
                yield "Tidak Lulus";
        };
        System.out.println(ucapan);

    }

}
