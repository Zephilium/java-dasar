package StudiKasus;

import java.util.Scanner;

public class ATM {
    public static void main(String[] args) {
        var saldo = 0;
        var ulang = true;

        while (ulang) {
            System.out.println("========== Menu ATM ==========");
            System.out.println("1. Cek Saldo");
            System.out.println("2. Tarik Saldo");
            System.out.println("3. Tambah Saldo");
            System.out.println("4. Keluar");
            System.out.print("Pilihan Anda : ");
            Scanner sc = new Scanner(System.in);
            int pilihan = sc.nextInt();

            switch (pilihan) {
                case 1:
                    cekSaldo(saldo);
                    break;
                case 2:
                    saldo = tarikSaldo(saldo);
                    break;
                case 3:
                    saldo = tambahSaldo(saldo);
                    break;
                case 4:
                    ulang = false;
                    break;
                default:
                    System.out.println("Pilihan salah, silahkan coba lagi");

            }

        }
        System.out.println("===== Program Selesai =====");

    }

    static void cekSaldo(int saldo) {
        System.out.println("Saldo Anda Adalah : " + saldo);
    }

    static int tarikSaldo(int saldo) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Masukkan saldo yang ingin di tarik : ");
        int tarik = sc.nextInt();
        if (tarik > saldo) {
            System.out.println("Saldo tidak boleh mines");
        } else {
            if (tarik % 50000 != 0) {
                System.out.println("Tarik saldo harus kelipatan 50rb");
            } else {
                return saldo -= tarik;
            }
        }
        return saldo;
    }

    static int tambahSaldo(int saldo) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Masukkan saldo yang ingin di tambah : ");
        int tambah = sc.nextInt();
        return saldo += tambah;
    }
}
